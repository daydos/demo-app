terraform {
 backend "s3" {
    encrypt = true
    # This bucket needs to be created manually
    bucket = "daydos-demo-terraform"
    # This DynamoDB table needs to be created manually
    dynamodb_table = "demo-terraform-lock"
    region = "eu-central-1"
    key = "tf.state"
 }
}