variable "aws_access_key_id" {}

variable "aws_secret_access_key" {}

variable "aws_region" {
  default = "eu-central-1"
}

variable "ecs_task_execution_role_name" {
  default = "demoEcsTaskExecutionRole"
}

variable "ecs_auto_scale_role_name" {
  default = "demoEcsAutoScaleRole"
}

variable "az_count" {
  default     = "2"
}

variable "app_repo_path" {
  default     = "248959607930.dkr.ecr.eu-central-1.amazonaws.com/demo-app"
}

variable "app_image_tag" {
  default     = "3fc39823"
}

variable "app_port" {
  default     = 80
}

variable "app_count" {
  default     = 1
}

variable "health_check_path" {
  default = "/status/ready"
}

variable "fargate_cpu" {
  default     = "1024"
}

variable "fargate_memory" {
  default     = "2048"
}
