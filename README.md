This repository contains a basic Python (Flask) web application in `/app` folder.

The application is dockerized via `DockerFile` and served by Nginx and UWSGI on
port 80 within the container.

`/terraform` folder contains all the necessary resources to deploy the docker 
image to AWS Fargate. 

The CI/CD config in `.gitlab-ci.yml` file works as below:

- Build the docker image for the web application
- Tag the docker image with short sha of the last commit
- Push the docker image to AWS Elastic Container Registry (ECR)
- Validate the terraform scripts
- Apply terraform scripts