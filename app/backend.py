from flask import Flask
from flask import jsonify
from flask import Response

from datetime import datetime

app = Flask(__name__)

start_time = datetime.now()

@app.route('/', methods=['GET'])
def index():
    return Response("Hello world", status=200, mimetype="text/plain")


@app.route('/status/alive', methods=['GET'])
def alive():
    return Response(None, status=200)


@app.route('/status/ready', methods=['GET'])
def ready():
    global start_time

    seconds_since_start = (datetime.now()-start_time).total_seconds()

    if seconds_since_start < 10:
        response = jsonify(ready=False)
        response.status_code=500
        response.mimetype = 'application/json'
        return response

    response = jsonify(ready=True)
    response.status_code=200
    response.mimetype = 'application/json'
    return response